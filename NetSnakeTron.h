#ifndef NetSnakeTron_H
#define NetSnakeTron_H

#include <string>
#include <iostream>
#include <SDL.h>
#include "Configuration.h"
#include "Server.h"
#include "Client.h"
#include "UserInterface.h"

class NetSnakeTron{
public:
	NetSnakeTron( Configuration &conf);
	~NetSnakeTron();
	NetSnakeTron &loop();
private:
	inline void log( const std::string &str, unsigned int type){ this->configuration.get_log().puts( str, type);}

	Configuration &configuration;
	Client *client;
	Server *server;
};

#endif//NetSnakeTron_H

