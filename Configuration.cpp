#include "Configuration.h"

Configuration::Configuration( const std::string &path): log( Log::ERROR | Log::WARNING | Log::DEPURATION | /*Log::TRACE |*/ Log::INFO),
															test( Log::INFO){
	const unsigned int NONE = 0x0, MODE = 0x1, PORT = 0x2, URL = 0x4, LOGN = 0x8, TESTN = 0x10;
	const unsigned int TPU = 0x20, PLAYERS = 0x40, GAMES = 0x80, ITERATIONS = 0x100, DIMENSIONS = 0x200;
	const unsigned int ALLOW_PAUSE = 0x400, BOT = 0x800, BLIND = 0x1000;
	unsigned int reads = NONE;
	std::ifstream file( path.c_str());	// android compiler doesn't support fstream( string)
	std::string line, tag, value;
	if( file.is_open()){
		while( !file.eof()){
			std::getline( file, line);
			size_t i = line.find_first_of( " \t");
			if( i != std::string::npos){
				tag = std::string( line.begin(), line.begin()+i);
				value = std::string( line.begin()+i+1, line.end());
			}else{
				tag = line;
				value = "";
			}
			if( tag == "MODE"){
				if( !(reads & MODE)){
					this->server = this->client = false;	// if the MODE value is not recognized both are false
					if( value == "server" || value == "double") this->server = true;
					if( value == "client" || value == "double") this->client = true;
					reads |= MODE;
				}
			}else if( tag == "PORT"){
				if( !(reads & PORT)){ this->port = std::stoi( value); reads |= PORT;}
			}else if( tag == "URL"){
				if( !(reads & URL)){ this->url = value; reads |= URL;}
			}else if( tag == "LOG_NAME"){
				if( !(reads & LOGN)){ this->log.setFile( value); reads |= LOGN;}
			}else if( tag == "TEST_NAME"){
				if( !(reads & TESTN)){ this->test.setFile( value); reads |= TESTN;}
			}else if( tag == "TIME_PER_UPDATE"){
				if( !(reads & TPU)){ this->timePerUpdate = std::stoi( value); reads |= TPU;}
			}else if( tag == "PLAYERS"){
				if( !(reads & PLAYERS)){ this->players = std::stoi( value); reads |= PLAYERS;}
			}else if( tag == "GAMES"){
				if( !(reads & GAMES)){ this->games = std::stoi( value); reads |= GAMES;}
			}else if( tag == "ITERATIONS"){
				if( !(reads & ITERATIONS)){ this->iterations = std::stoi( value); reads |= ITERATIONS;}
			}else if( tag == "DIMENSIONS"){
				if( !(reads & DIMENSIONS)){
					this->W = std::stoi( value);
					i = value.find_first_of( " \t");	// if( i == npos) throw "wrong dimension format.";
					value = std::string( value.begin()+i+1, value.end());
					this->H = std::stoi( value);
					reads |= DIMENSIONS;
				}
			}else if( tag == "ALLOW_PAUSE"){
				if( !(reads & ALLOW_PAUSE)){ this->allowPause = value == "yes"; reads |= ALLOW_PAUSE;}
			}else if( tag == "BOT"){
				if( !(reads & BOT)){ this->bot = value == "yes"; reads |= BOT;}
			}else if( tag == "BLIND"){
				if( !(reads & BLIND)){ this->blind = value == "yes"; reads |= BLIND;}
			}
		}
	}
	if( !(reads & MODE)){
		this->server = true;
		this->client = false;
	}
	if( !(reads & PORT))	this->port = 9999;
	if( !(reads & URL))
		this->url = ( this->server ? "localhost" : "snaketron.ddns.net");	// allow MODE == "double" && URL != "localhost" ??
	if( !(reads & LOGN)){
		this->log.setFile( this->server ? (this->client ? "Double.log" : "Server.log")
						: (this->client ? "Client.log": "Unrecognized.log"));
		if( !this->server && !this->client)
			this->log.puts( "WARNING: Configuration::Configuration: MODE value unrecognized.", Log::WARNING);
	}
	if( !(reads & TESTN))	this->test.setFile( "test.log");
	if( !(reads & TPU))	this->timePerUpdate = 500;
	if( !(reads & PLAYERS))	this->players = 3;
	if( !(reads & GAMES))	this->games = ( this->client ? 1 : 0);
	if( !(reads & ITERATIONS)) this->iterations = 0;
	if( !(reads & DIMENSIONS)){
		this->W = 64;
		this->H = 48;
	}
	if( !(reads & ALLOW_PAUSE))	this->allowPause = false;
	if( !(reads & BOT))	this->bot = false;
	if( !(reads & BLIND))	this->blind = false;
	if( file.is_open()) file.close();
	else this->save( path);
}

Configuration &Configuration::save( const std::string &path){
	std::ofstream file( path.c_str());	// android compiler doesn't support fstream( string)
	if( file.is_open()){
		file << "MODE " << (this->server ? (this->client ? "double" : "server")
						: (this->client ? "client" : "WARNING unrecognized")) << std::endl;
		file << "PORT " << this->port << std::endl;
		file << "URL " << this->url << std::endl;
		file << "LOG_NAME " << this->log.getFile() << std::endl;
		file << "TEST_NAME " << this->test.getFile() << std::endl;
		file << "TIME_PER_UPDATE " << this->timePerUpdate << std::endl;
		file << "PLAYERS " << this->players << std::endl;
		file << "GAMES " << this->games << std::endl;
		file << "ITERATIONS " << this->iterations << std::endl;
		file << "DIMENSIONS " << this->W << " " << this->H << std::endl;
		file << "ALLOW_PAUSE " << (this->allowPause ? "yes" : "no") << std::endl;
		file << "BOT " << (this->bot ? "yes" : "no") << std::endl;
		file << "BLIND " << (this->blind ? "yes" : "no") << std::endl;
		file.close();
	} else this->log.puts( "could not access to the configuration file: path", Log::ERROR);
}

