#include "ClientHandler.h"

ClientHandler::ClientHandler( const std::string &n, TCPsocket c, Configuration &conf): state( ClientHandler::SOCKET_DISCONNECTED),
			exit_signal(false), configuration(conf), name(n), client(c), clientSocketSet(nullptr){
	this->log( "ClientHandler::ClientHandler().\n", Log::TRACE);
	this->stateMutex = SDL_CreateMutex();
	this->inputMutex = SDL_CreateMutex();
	if( this->client != nullptr){
		this->state = ClientHandler::SOCKET_CONNECTED;
		this->clientSocketSet = SDLNet_AllocSocketSet(1);
		SDLNet_TCP_AddSocket( this->clientSocketSet, this->client);
	}
	this->thread = SDL_CreateThread( ClientHandler::run, this->name.c_str(), (void*)this);
}

ClientHandler::~ClientHandler(){
	this->log( "ClientHandler::~ClientHandler().\n", Log::TRACE);
	if( this->thread != nullptr) this->waitThread();
	if( this->stateMutex != nullptr) SDL_DestroyMutex( this->stateMutex);
	if( this->inputMutex != nullptr) SDL_DestroyMutex( this->inputMutex);
}


ClientHandler &ClientHandler::setName( const std::string &name){
	this->log( "ClientHandler::setName().\n", Log::TRACE);
	this->name = name;
	return *this;
}

ClientHandler &ClientHandler::setSocket( TCPsocket socket){
	this->log( "ClientHandler::setSocket().\n", Log::TRACE);
	if( socket != nullptr){
		this->client = socket;
		this->state = ClientHandler::SOCKET_CONNECTED;
		this->clientSocketSet = SDLNet_AllocSocketSet(1);
		SDLNet_TCP_AddSocket( this->clientSocketSet, this->client);
	}
	return *this;
}

bool ClientHandler::is_socket_connected(){
	return this->state != ClientHandler::SOCKET_DISCONNECTED;
}

bool ClientHandler::isInputReady(){
	return !this->input.empty();
}

std::string ClientHandler::getInput(){
	this->log( "ClientHandler::getInput().\n", Log::TRACE);
	std::string input;
	if( SDL_LockMutex( this->inputMutex) == 0){
		if( this->input.empty())
			input = "";
		else{
			input = this->input.front();
			this->input.pop();
		}
		SDL_UnlockMutex( this->inputMutex);
	}else this->log( "ClientHandler::getInput(): Warning, could not lock the input mutex.\n", Log::WARNING);
	return input;
}

ClientHandler &ClientHandler::addOutput( const std::string &output){
	this->log( "ClientHandler::addOutput().\n", Log::TRACE);
	if( SDL_LockMutex( this->stateMutex) == 0){
		if( this->state != ClientHandler::SOCKET_DISCONNECTED){
			this->output.push( output);
			this->state = ClientHandler::OUTPUT_READY;
			SDL_UnlockMutex( this->stateMutex);
		}else{
			SDL_UnlockMutex( this->stateMutex);
			this->log( "ClientHandler::addOutput(): Warning, SOCKET_DISCONNECTED.\n", Log::WARNING);
		}
	}else this->log( "ClientHandler::addOutput(): Warning, could not lock the state mutex.\n", Log::WARNING);
	return *this;
}

ClientHandler &ClientHandler::waitThread(){
	this->log( "ClientHandler::waitThread().\n", Log::TRACE);
	if( this->thread != nullptr){
		this->exit_signal = true;
		SDL_WaitThread( this->thread, nullptr);
		this->thread = nullptr;
	}else this->log( "ClientHandler::waitThread(): there is no thread to wait.\n", Log::WARNING);
	return *this;
}

int ClientHandler::run( void *data){		ClientHandler *virtual_this = (ClientHandler*)data;
if(SDL_LockMutex( virtual_this->stateMutex) == 0){
if( virtual_this->state != ClientHandler::SOCKET_DISCONNECTED){
	SDL_UnlockMutex( virtual_this->stateMutex);
	std::string starting( "ClientHandler::run(): Starting the client handler: ");
	virtual_this->log( starting + virtual_this->name + "\n", Log::INFO);
	const unsigned int LengthBufferInput = 1024;
	char input[LengthBufferInput];
	int readBytes = 1, i, j;
	while( readBytes > 0 && !(virtual_this->exit_signal)){
		if( readBytes > 0 && SDLNet_CheckSockets( virtual_this->clientSocketSet, 0) > 0){
			while( SDLNet_SocketReady( virtual_this->client) != 0){
				readBytes = SDLNet_TCP_Recv( virtual_this->client, input, LengthBufferInput);
				for( i=j=0; i<readBytes; i++) if( input[i] == '\0'){
					virtual_this->processInput( input + j);
					j = i+1;
				}
				if( j != i)
					virtual_this->processInput( input + j);
			}
		}
		if( readBytes > 0){
			if(SDL_LockMutex( virtual_this->stateMutex) == 0){
				if( virtual_this->state == ClientHandler::OUTPUT_READY){
					while( !virtual_this->output.empty()){
						std::string tmp = virtual_this->output.front();
						if( SDLNet_TCP_Send( virtual_this->client, tmp.c_str(), tmp.size()+1) < tmp.size()+1)
							virtual_this->log( "ClientHandler::run(): Warning, error sending.\n", Log::INFO);
						virtual_this->output.pop();
					}
					virtual_this->state = ClientHandler::SOCKET_CONNECTED;
				}
				SDL_UnlockMutex( virtual_this->stateMutex);
			}else virtual_this->log( "ClientHandler::run(): Warning, could not lock the state mutex.\n", Log::WARNING);
		}
		SDL_Delay(0);
	}
	virtual_this->state = ClientHandler::SOCKET_DISCONNECTED;	//
	SDLNet_TCP_DelSocket( virtual_this->clientSocketSet, virtual_this->client);
	SDLNet_FreeSocketSet( virtual_this->clientSocketSet);
	SDLNet_TCP_Close( virtual_this->client);
	std::string stopped( "ClientHandler::run(): Stopped the client handler: ");
	virtual_this->log( stopped + SDL_GetThreadName( virtual_this->thread) + "\n", Log::INFO);
}else{	// (virtual_this->state != ClientHandler::SOCKET_DISCONNECTED)
	SDL_UnlockMutex( virtual_this->stateMutex);
	virtual_this->log( "ClientHandler::run(): Warning, the client socket is not connected.\n", Log::WARNING);
}
}else	// (SDL_LockMutex( virtual_this->stateMutex) == 0)
	virtual_this->log( "ClientHandler::run()-entering: Warning, could not lock the state mutex.\n", Log::WARNING);
	return 0;
}

void ClientHandler::processInput( const std::string &input){
	this->log( "ClientHandler::processInput().\n", Log::TRACE);
	if( SDL_LockMutex( this->inputMutex) == 0){
		if( !this->configuration.getAllowPause() || input != "<pause>")
			this->input.push( input);
		else this->input.push( "<>");
		SDL_UnlockMutex( this->inputMutex);
		this->log( std::string( "ClientHandler::processInput(): input: ") + input + ".\n", Log::INFO);
	}else this->log( "ClientHandler::processInput(): Warning, could not lock the input mutex.\n", Log::WARNING);
}


