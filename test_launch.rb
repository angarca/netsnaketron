`rm localhost.* *.conf *.log *.test`

#6.times do |i| players = (i+1) * 5
players = 5
iterations = 100
20.times do |i|
	games = i+1

	File.open( "server.conf", 'w') do |f|
		f.puts 'MODE server'
		f.puts 'PORT 9999'
		f.puts 'LOG_NAME server.log'
		f.puts 'TIME_PER_UPDATE 100'
		f.puts "PLAYERS #{players}"
		f.puts "GAMES #{games}"
		f.puts "ITERATIONS #{iterations}"
		f.puts 'DIMENSIONS 64 48'
		f.puts 'ALLOW_PAUSE no'
	end
#	(players*games).times do |i|
#		File.open( "bot#{i}.conf", 'w') do |f|
#			f.puts 'MODE client'
#			f.puts 'PORT 9999'
#			f.puts 'URL localhost'
#			f.puts "LOG_NAME bot#{i}.log"
#			f.puts "TEST_NAME bot#{i}.test"
#			f.puts 'BOT yes'
#			f.puts 'BLIND yes'
#		end
#	end
	File.open( "bots.conf", 'w') do |f|
		f.puts 'MODE client'
		f.puts 'PORT 9999'
		f.puts 'URL localhost'
		f.puts "LOG_NAME bots.log"
		f.puts 'TEST_NAME bots.test'
		f.puts 'BOT yes'
		f.puts 'BLIND yes'
	end

	server = Thread.new {
		`./main server.conf`
		n = 0.0
		p = 0
		File.open( 'bots.test', 'r') do |f|
			while !f.eof? do
				n += f.gets.to_f
				p += 1
			end
		end
#		if p != players * games * iterations then STDERR.puts "wrong, #{p} != #{players} * #{games} * #{iterations}." end
		n /= p
		File.open( "localhost.#{games}.games", 'w+') do |f|
			f.puts "#{players} #{n}"
		end
		File.open( "localhost.#{players}.players", 'w+') do |f|
			f.puts "#{games} #{n}"
		end
		puts 'End.'
	}
	sleep 1
	(players*games).times do |i|
		Thread.new { `./main bots.conf`}
	end

	server.join

end
