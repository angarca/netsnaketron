#ifndef NetSnakeTron_Client_H
#define NetSnakeTron_Client_H

#include <iostream>

#include <string>
#include <SDL.h>
#include <SDL_net.h>
#include "Configuration.h"
#include "EventSender.h"
#include "StateHandler.h"

class Client{
public:
	static const unsigned int NONE = 0x0, IS_SERVER_IP_INITIALIZED = 0x1, IS_SERVER_SOCKET_CONECTED = 0x2, EXIT_SIGNAL = 0x4;

	Client( Configuration &conf);
	~Client();
	Client &resolve( const std::string &ip, unsigned int port);
	Client &startConection();
	Client &stopConection();
	EventSender &getEventSender();
	StateHandler &getStateHandler();
	bool is_exiting();

	static int serverHandler( void *data);
private:
	void processInput( const std::string &input);
	inline void log( const std::string &str, unsigned int type){ this->configuration.get_log().puts( str, type);}

	bool negotiation_done;
	unsigned int state;
	Configuration &configuration;
	IPaddress serverIP;
	TCPsocket serverTCPSocket;
	SDLNet_SocketSet serverSocketSet;
	SDL_Thread *serverHandlerThread;
	EventSender eventSender;
	StateHandler stateHandler;

	std::string *inputs_V;
	unsigned int inputs_N;
	Uint32 time;
};

#endif//NetSnakeTron_Client_H

