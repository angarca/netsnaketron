#ifndef NetSnakeTron_EventSender_H
#define NetSnakeTron_EventSender_H

#include <string>
#include <SDL.h>
#include "Configuration.h"
#include "GameState.h"

class EventSender{
public:
	EventSender( Configuration &conf);
	~EventSender();
	bool isReady();
	EventSender &setReady( bool b = true);
	std::string getMessage();
	EventSender &setMessage( const std::string &message);
private:
	inline void log( const std::string &str, unsigned int type){ this->configuration.get_log().puts( str, type);}

	Configuration &configuration;
	bool ready;
	std::string message;
	SDL_mutex *mutex;
};

#endif//NetSnakeTron_EventSender_H

