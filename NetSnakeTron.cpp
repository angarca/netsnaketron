#include "NetSnakeTron.h"

NetSnakeTron::NetSnakeTron( Configuration &conf): configuration(conf){

	SDL_Init( SDL_INIT_TIMER);
	if( SDLNet_Init() == -1){
		this->log( std::string( "NetSnakeTron::NetSnakeTron(): Error, ") + SDLNet_GetError(), Log::ERROR);
		exit(-1);
	}

	this->log( "NetSnakeTron().\n", Log::TRACE);
	this->server = nullptr;
	this->client = nullptr;
	if( this->configuration.is_server()){
		this->log( "\n\nStarting as server.\n\n", Log::INFO);
		this->server = new Server( this->configuration);
		this->server->resolve( this->configuration.get_port());
		this->server->startListen();
	}if ( this->configuration.is_client()){
		this->log( "\n\nStarting as client.\n\n", Log::INFO);
		this->client = new Client( this->configuration);
		this->client->resolve( this->configuration.get_url(), this->configuration.get_port());
		this->client->startConection();
	}

}

NetSnakeTron::~NetSnakeTron(){
	if( this->configuration.is_client()){
		this->client->stopConection();
		delete this->client;
	}
	if( this->configuration.is_server()){
		this->server->stopListen();
		delete this->server;
	}

	this->log( "\n\nStoping.\n\n", Log::INFO);

	SDLNet_Quit();
	SDL_Quit();
}

NetSnakeTron &NetSnakeTron::loop(){
	if( this->configuration.is_client()){
		if( this->configuration.getBlind())
			while( !this->client->is_exiting()) SDL_Delay(1);
		else{
			UserInterface UI( this->client->getEventSender(), this->client->getStateHandler(), this->configuration);
			UI.loop();
		}
	} else if( this->configuration.is_server()){
		if( this->configuration.get_games() == 0){
			std::string tmp;
			do{
				std::cin >> tmp;
			}while( tmp != "exit");
		}else while( !this->server->is_exiting()) SDL_Delay(1);
	}
	return *this;
}

