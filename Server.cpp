#include "Server.h"

Server::Server( Configuration &conf): state(Server::NONE), configuration(conf), inputTCPSocket(nullptr), clientListenerThread(nullptr){
	this->log( "Server::Server().\n", Log::TRACE);
}

Server::~Server(){
	this->log( "Server::~Server().\n", Log::TRACE);
	if( this->state & Server::IS_INPUT_SOCKET_LISTENING)
		this->stopListen();
}

Server &Server::resolve( unsigned int port){
	this->log( "Server::resolve(): Trying to set up the server configuration.\n", Log::TRACE);
	SDLNet_ResolveHost( &(this->inputIP), nullptr, port);
	this->state |= Server::IS_INPUT_IP_INITIALIZED;
}

Server &Server::startListen(){
	this->log( "Server::startListen(): Trying to start listen on server.\n", Log::TRACE);
	if( this->state & Server::IS_INPUT_IP_INITIALIZED){
		this->inputTCPSocket = SDLNet_TCP_Open( &(this->inputIP));
		this->clientListenerThread = SDL_CreateThread( Server::clientListener, "Client listener thread", (void*)this);
		this->state |= Server::IS_INPUT_SOCKET_LISTENING;
	}else this->log( "Server::startListen(): Warning, the server configuration is not set up.\n", Log::WARNING);
}

Server &Server::stopListen(){
	this->log( "Server::stopListen(): Trying to stop listen on server.\n", Log::TRACE);
	if( this->state & Server::IS_INPUT_SOCKET_LISTENING){
		this->state |= Server::EXIT_SIGNAL;
		SDL_WaitThread( this->clientListenerThread, nullptr);
	}else this->log( "Server::stopListen(): Warning, the server wasn't listening.\n", Log::WARNING);
}

bool Server::is_exiting(){
	return this->state & Server::EXIT_SIGNAL;
}

int Server::clientListener( void *data){	Server *virtual_this = (Server*)data;
	virtual_this->log( "Server::clientListener(): Starting client listener.\n", Log::INFO);
	unsigned int MAX_GAMES = virtual_this->configuration.get_games();
	bool last_game_on = false;
	std::string newName;
	TCPsocket newClient;
	ClientHandler *newClientHandler = nullptr;
	GameHandler *newGame = new GameHandler( virtual_this->configuration);
	std::vector< GameHandler*> games;
	games.push_back( newGame);
	unsigned int nextClient = 0, nextGame = 1;
	while( !(last_game_on || virtual_this->state & Server::EXIT_SIGNAL)){
		newClient = SDLNet_TCP_Accept( virtual_this->inputTCPSocket);
		if( newClient != nullptr){
			newName = std::string("Client handler nº ") + std::to_string(nextClient);
			newClientHandler = new ClientHandler( newName, newClient, virtual_this->configuration);
			newGame->addClient( newClientHandler);
			if( ++nextClient % virtual_this->configuration.get_players() == 0){
				newGame->start();
				if( MAX_GAMES == 0 || nextGame < MAX_GAMES){
					newGame = new GameHandler( virtual_this->configuration);
					games.push_back( newGame);
					nextGame++;
				} else last_game_on = true;
			}
		}else SDL_Delay( 1);
	}
	while( !( virtual_this->state & Server::EXIT_SIGNAL)){
		std::vector< GameHandler*>::iterator it;
		for( last_game_on = false, it = games.begin(); it != games.end(); it++)
			last_game_on = last_game_on || (*it)->is_on();
		if( !last_game_on)
			virtual_this->state |= Server::EXIT_SIGNAL;
	}
	for( std::vector< GameHandler*>::iterator it = games.begin(); it != games.end(); it++){
		(*it)->waitThread();
		delete *it;
	}
	SDLNet_TCP_Close( virtual_this->inputTCPSocket);
	virtual_this->state &= ~( Server::IS_INPUT_SOCKET_LISTENING);
	virtual_this->log( "Server::clientListener(): Stopped client listener.\n", Log::INFO);
	return 0;
}

