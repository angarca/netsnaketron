#ifndef NetSnakeTron_GameState_H
#define NetSnakeTron_GameState_H

#include <vector>
#include <deque>
#include <string>
#include <algorithm>
#include <SDL.h>		// used only for random generation (SDL_GetTicks), REPLACE
#include "Configuration.h"

class GameState{
public:
	static const unsigned int RED = 0xff0000, GREEN = 0xff00, BLUE = 0xff, BLACK = 0x0;
	static const unsigned int SKY = 0xffff, BROWN = 0xff00ff, YELLOW = 0xffff00, WHITE = 0xffffff;
	static const unsigned int LIGHT_RED = 0xff7f7f, LIGHT_GREEN = 0x7fff7f, LIGHT_BLUE = 0x7f7fff, GRAY = 0x7f7f7f;
	static const unsigned int LIGHT_SKY = 0x7fffff, LIGHT_BROWN = 0xff7fff, LIGHT_YELLOW = 0xffff7f;
	static const unsigned int DARK_RED = 0x7f0000, DARK_GREEN = 0x7f00, DARK_BLUE = 0x7f;
	static const unsigned int DARK_SKY = 0x7f7f, DARK_BROWN = 0x7f007f, DARK_YELLOW = 0x7f7f00;
	static const unsigned int BODY_COLORS[5];
	static const unsigned int HEAD_COLORS[5];
	static const unsigned int SELF_HEAD_COLORS[5];

	GameState( Configuration &conf);
	~GameState();
	std::string configurationSummary();
	std::string addPlayer( const std::string &requestConfiguration, std::string &newPlayerMessage);
	GameState &initializePlayer( const std::string &grantedConfiguration);	// Plus current state summary
	static std::string update( /*const*/std::vector< std::string> &events);
	GameState &update( const std::string &update);
	std::string generateApple();
	GameState &getSize( unsigned int &w, unsigned int &h);
	GameState &getDisplay( unsigned int **display, unsigned int w, unsigned int h);
	bool isPaused();

	static void overrideInput( std::string &bufferInput, const std::string &newInput, bool allowPause);
private:
	inline void log( const std::string &str, unsigned int type){ this->configuration.get_log().puts( str, type);}

	struct Pos{
		unsigned int x, y;
		Pos( unsigned int X = 0, unsigned int Y = 0):x(X),y(Y){}
		bool operator==( const Pos &other) const { return this->x == other.x && this->y == other.y;}
	};
	struct Player{
		std::deque< GameState::Pos> body; std::string direction; GameState::Pos pos; unsigned int color, headColor, growth;
		bool viewer;
		bool collide( const GameState::Pos &pos){
			bool b;
			if( this->viewer) b = false;
			else{
				b = std::find( body.begin(), body.end(), pos) != body.end();
				if( &(this->pos) != &pos) b = b || (pos == this->pos);
			}
			return b;
		}
		std::string describe(){
			std::string description = "<player ";
			description += std::to_string( this->pos.x) + " " + std::to_string( this->pos.y) + " ";
			description += std::to_string( this->color) + " " + std::to_string( this->headColor) + ">";
			return 	description;
		}
	};

	inline void setApple( const std::string &description, size_t &pos);
	inline void addPlayer( const std::string &description, size_t &pos);
	inline bool collide( const GameState::Pos &pos);

	bool pause, apple_update;
	Configuration &configuration;
	GameState::Pos apple, dim;
	unsigned int appleColor;
	std::vector< GameState::Player> players;

	// bot
	unsigned int bot_t;
};

#endif//NetSnakeTron_GameState_H

