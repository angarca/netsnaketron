#ifndef NetSnakeTron_ClientHandler_H
#define NetSnakeTron_ClientHandler_H

#include <queue>
#include <string>
#include <SDL.h>
#include <SDL_net.h>
#include "Configuration.h"
#include "GameState.h"

class ClientHandler{
public:
	ClientHandler( const std::string &n, TCPsocket c, Configuration &conf);
	~ClientHandler();
	ClientHandler &setName( const std::string &name);
	ClientHandler &setSocket( TCPsocket socket);
	bool is_socket_connected();
	bool isInputReady();
	std::string getInput();
	ClientHandler &addOutput( const std::string &output);
	ClientHandler &waitThread();

	static int run( void *data);
private:
	void processInput( const std::string &input);
	inline void log( const std::string &str, unsigned int type){ this->configuration.get_log().puts( str, type);}

	enum{ SOCKET_DISCONNECTED, SOCKET_CONNECTED, OUTPUT_READY} state;
	SDL_mutex *stateMutex, *inputMutex;

	bool exit_signal;
	Configuration &configuration;
	std::string name;
	std::queue< std::string> input, output;
	TCPsocket client;
	SDLNet_SocketSet clientSocketSet;
	SDL_Thread *thread;
};

#endif//NetSnakeTron_ClientHandler_H
