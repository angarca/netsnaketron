#include "StateHandler.h"

StateHandler::StateHandler( Configuration &conf): configuration(conf), stateDisplayable(0), stateUpdateable(1){
	this->log( "StateHandler::StateHandler().\n", Log::TRACE);
	this->mutex = SDL_CreateMutex();
	this->gameState[0] = new GameState( this->configuration);
	this->gameState[1] = new GameState( this->configuration);
}

StateHandler::~StateHandler(){
	this->log( "StateHandler::~StateHandler().\n", Log::TRACE);
	if( this->gameState[0] != nullptr) delete this->gameState[0];
	if( this->gameState[1] != nullptr) delete this->gameState[1];
	if( this->mutex != nullptr) SDL_DestroyMutex( this->mutex);
}

std::string StateHandler::configurationSummary(){
	return this->gameState[0]->configurationSummary();
}

StateHandler &StateHandler::initializePlayer( const std::string &grantedConfiguration){
	this->gameState[0]->initializePlayer( grantedConfiguration);
	this->gameState[1]->initializePlayer( grantedConfiguration);
	return *this;
}

StateHandler &StateHandler::getSize( unsigned int &w, unsigned int &h){
	this->log( "StateHandler::getSize().\n", Log::TRACE);
	if( SDL_LockMutex( this->mutex) == 0){
		this->gameState[ this->stateDisplayable]->getSize( w, h);
		SDL_UnlockMutex( this->mutex);
	}else this->log( "StateHandler::getSize(): Warning, the mutex could not be locked.\n", Log::WARNING);
	return *this;
}

StateHandler &StateHandler::getDisplay( unsigned int **display, unsigned int w, unsigned int h){
	this->log( "StateHandler::getDisplay().\n", Log::TRACE);
	if( SDL_LockMutex( this->mutex) == 0){
		this->gameState[ this->stateDisplayable]->getDisplay( display, w, h);
		SDL_UnlockMutex( this->mutex);
	}else this->log( "StateHandler::getDisplay(): Warning, the mutex could not be locked.\n", Log::WARNING);
	return *this;
}

StateHandler &StateHandler::setState( const std::string &update){
	this->log( "StateHandler::setState().\n", Log::TRACE);
	this->gameState[ this->stateUpdateable]->update( update);
	if( SDL_LockMutex( this->mutex) == 0){
		unsigned int tmp = this->stateDisplayable;
		this->stateDisplayable = this->stateUpdateable;
		this->stateUpdateable = tmp;
		SDL_UnlockMutex( this->mutex);
	}else this->log( "StateHandler::setState(): Warning, the mutex could not be locked.\n", Log::WARNING);	
	this->gameState[ this->stateUpdateable]->update( update);
	return *this;
}


