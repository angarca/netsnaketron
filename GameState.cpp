#include "GameState.h"

const unsigned int GameState::BODY_COLORS[5] = { GameState::RED, GameState::SKY, GameState::BROWN, GameState::YELLOW, GameState::BLUE};
const unsigned int GameState::HEAD_COLORS[5] = { GameState::DARK_RED, GameState::DARK_SKY, GameState::DARK_BROWN,
											GameState::DARK_YELLOW, GameState::DARK_BLUE};
const unsigned int GameState::SELF_HEAD_COLORS[5] = { GameState::LIGHT_RED, GameState::LIGHT_SKY,
								GameState::LIGHT_BROWN, GameState::LIGHT_YELLOW, GameState::LIGHT_BLUE};

GameState::GameState( Configuration &conf): pause(true), apple_update(false), configuration(conf){
	this->log( "GameState::GameState().\n", Log::TRACE);
	SDL_Delay(1); SDL_Delay(1); SDL_Delay(1); SDL_Delay(1);	// To force randomness on getTicks; equivalent to srand
	this->configuration.get_dimensions( this->dim.x, this->dim.y);
	this->apple.x = SDL_GetTicks() % this->dim.x;
	this->apple.y = SDL_GetTicks() % this->dim.y;
	this->appleColor = GameState::LIGHT_GREEN;

// bot
	this->bot_t = 0;
}

GameState::~GameState(){
	this->log( "GameState::~GameState().\n", Log::TRACE);
}

std::string GameState::configurationSummary(){
	return "";
}

std::string GameState::addPlayer( const std::string &requestConfiguration, std::string &newPlayerMessage){
	bool occupied;
	GameState::Player newPlayer;
	std::string grantedConfiguration, currentState;

	grantedConfiguration = "<grantedConfiguration ";
	grantedConfiguration += "<dimensions " + std::to_string( this->dim.x) + " " + std::to_string( this->dim.y) + ">";
	grantedConfiguration += ">";

	currentState = "<currentState ";
	currentState += "<apple " + std::to_string( this->apple.x) + " " + std::to_string( this->apple.y);
	currentState += " " + std::to_string( this->appleColor) + ">";
	for( std::vector< GameState::Player>::iterator it = this->players.begin(); it != this->players.end(); it++){
		currentState += it->describe();
	}

	do{
		newPlayer.pos.x = SDL_GetTicks() % this->dim.x;
		newPlayer.pos.y = SDL_GetTicks() % this->dim.y;
		occupied = this->collide( newPlayer.pos);
	}while( occupied);

	newPlayer.direction = "<up>";
	newPlayer.color = GameState::BODY_COLORS[ this->players.size() % 5];
	newPlayer.headColor = GameState::HEAD_COLORS[ this->players.size() % 5];
	newPlayer.growth = 10;
	newPlayer.viewer = false;
	this->players.push_back( newPlayer);

	newPlayerMessage = newPlayer.describe();
	newPlayer.headColor = GameState::SELF_HEAD_COLORS[ (this->players.size() - 1) % 5];
	currentState += newPlayer.describe();
	currentState += ">";

	return grantedConfiguration + currentState;
}

GameState &GameState::initializePlayer( const std::string &grantedConfiguration){
	size_t pos;
	GameState::Player newPlayer;
	pos = grantedConfiguration.find( "<grantedConfiguration");
	pos = grantedConfiguration.find( "<dimensions", pos);
	pos = grantedConfiguration.find_first_of( "0123456789", pos);
	this->dim.x = std::stoi( std::string( grantedConfiguration.begin() + pos, grantedConfiguration.end()));
	pos = grantedConfiguration.find_first_of( " \t", pos);
	pos = grantedConfiguration.find_first_of( "0123456789", pos);
	this->dim.y = std::stoi( std::string( grantedConfiguration.begin() + pos, grantedConfiguration.end()));
	pos = grantedConfiguration.find( ">", pos);
	pos = grantedConfiguration.find( ">", pos+1);
	pos = grantedConfiguration.find( "<currentState", pos);
	pos = grantedConfiguration.find( "<apple", pos);
	this->setApple( grantedConfiguration, pos);
	while( (pos = grantedConfiguration.find( "<player", pos)) != std::string::npos)
		this->addPlayer( grantedConfiguration, pos);
	return *this;
}

std::string GameState::update( std::vector< std::string> &events){
	bool pause = false;
	std::string message;
	std::vector< std::string>::iterator it;
	for( it = events.begin(); !pause && it != events.end(); it++){
		if( *it != "<pause>")
			message += *it;
		else pause = true;
	}
	if( pause) message = "<pause>";
	return message;
}

std::string GameState::generateApple(){
	std::string apple;
	if( this->apple_update){
		apple = std::string( "<apple ") + std::to_string( this->apple.x) + " " + std::to_string( this->apple.y) + " " +
											std::to_string( this->appleColor) + ">";
		this->apple_update = false;
	}
	return apple;
}

GameState &GameState::update( const std::string &update){
	this->log( "GameState::update(string).\n", Log::TRACE);
	this->log( std::string( "GameState::update(string): string: ") + update + ".\n", Log::INFO);
	size_t pos;
	if( (pos = update.find( "<player")) != std::string::npos)
		this->addPlayer( update, pos);
	else if( update == "<pause>") this->pause =! this->pause;
	else if( this->pause);
	else{
		// bot
		if( this->configuration.getBot()){
			unsigned int m;
			SDL_Event sdlevent;
			sdlevent.type = SDL_KEYDOWN;
			if( ( m = this->bot_t++ % this->dim.x) == 0)
				sdlevent.key.keysym.sym = SDLK_DOWN;
			else if( m == 1)
				sdlevent.key.keysym.sym = SDLK_LEFT;
			SDL_PushEvent(&sdlevent);
		}// end bot
		size_t b = 0, e;
		std::vector< GameState::Player>::iterator player;
		std::string tmp;
		for( player = this->players.begin(); player != this->players.end(); player++){
			e = update.find( ">", b) + 1;
			tmp = std::string( update.begin() + b, update.begin() + e);
			b = e;
			player->body.push_back( player->pos);
			if( player->direction == "<up>" || player->direction == "<down>"){
				if( tmp == "<left>" || tmp == "<right>")
					player->direction = tmp;
			}else if( player->direction == "<left>" || player->direction == "<right>"){
				if( tmp == "<up>" || tmp == "<down>")
					player->direction = tmp;
			}
			if( player->direction == "<up>")
				player->pos.y = (player->pos.y + this->dim.y - 1) % this->dim.y;
			else if( player->direction == "<down>")
				player->pos.y = (player->pos.y + this->dim.y + 1) % this->dim.y;
			else if( player->direction == "<left>")
				player->pos.x = (player->pos.x + this->dim.x - 1) % this->dim.x;
			else if( player->direction == "<right>")
				player->pos.x = (player->pos.x + this->dim.x + 1) % this->dim.x;
			if( !player->viewer && player->pos == this->apple){
				if( this->configuration.is_server()){
					bool ocuppied;
					do{
						this->apple.x = SDL_GetTicks() % this->dim.x;
						this->apple.y = SDL_GetTicks() % this->dim.y;
						ocuppied = this->collide( this->apple);
					}while( ocuppied);
				}
				this->apple_update = true;
				player->growth += 5;
			}
			if( player->growth > 0) player->growth--;
			else player->body.pop_front();
		}
		if( this->configuration.is_client()){
			if( (b = update.find( "<apple", b)) != std::string::npos){
				if( !this->apple_update)
					this->log( "GameState::update(): Warning, found unexpected apple message.\n", Log::WARNING);
				else this->apple_update = false;
				this->setApple( update, b);
			}else if( this->apple_update)
				this->log( "GameState::update(): Warning, apple message required and not foud.\n", Log::WARNING);
		}
		bool *colliding = new bool[ this->players.size()];	unsigned int i = 0;
		for( player = this->players.begin(); player != this->players.end(); player++, i++){
			if( player->viewer) colliding[i] = false;
			else colliding[i] = this->collide( player->pos);
		}
		i = 0;
		for( player = this->players.begin(); player != this->players.end(); player++, i++){
			if( colliding[i]){
				if( player->body.size() > 0){
					player->pos = player->body.back();
					player->body.pop_back();
				}else player->viewer = true;
			}
		}
		delete colliding;
	}
	return *this;
}

GameState &GameState::getSize( unsigned int &w, unsigned int &h){
	this->log( "GameState::getSize().\n", Log::TRACE);
	w = this->dim.x; h = this->dim.y;
	return *this;
}

GameState &GameState::getDisplay( unsigned int **display, unsigned int w, unsigned int h){
	this->log( "GameState::getDisplay().\n", Log::TRACE);
	unsigned int i,j;
	std::vector< GameState::Player>::iterator player;
	for( i=0; i<w; i++) for( j=0; j<h; j++)
		display[i][j] = GameState::BLACK;
	for( player = this->players.begin(); player != this->players.end(); player++) if( !player->viewer)
		for( std::deque< GameState::Pos>::iterator block = player->body.begin(); block != player->body.end(); block++)
			display[ block->x][ block->y] = player->color;
	for( player = this->players.begin(); player != this->players.end(); player++) if( !player->viewer)
		display[ player->pos.x][ player->pos.y] = player->headColor;
	display[ this->apple.x][ this->apple.y] = GameState::GREEN;
	return *this;
}

bool GameState::isPaused(){
	this->log( "GameState::isPaused().\n", Log::TRACE);
	return this->pause;
}

void GameState::overrideInput( std::string &bufferInput, const std::string &newInput, bool allowPause){
	if( bufferInput != "<pause>")
		if( allowPause || newInput != "<pause>")
			bufferInput = newInput;
}

void GameState::setApple( const std::string &description, size_t &pos){
	pos = description.find_first_of( "0123456789", pos);
	this->apple.x = std::stoi( std::string( description.begin() + pos, description.end()));
	pos = description.find_first_of( " \t", pos);	// pos += string.size()	
	pos = description.find_first_of( "0123456789", pos);
	this->apple.y = std::stoi( std::string( description.begin() + pos, description.end()));
	pos = description.find_first_of( " \t", pos);
	pos = description.find_first_of( "0123456789", pos);
	this->appleColor = std::stoi( std::string( description.begin() + pos, description.end()));
	pos = description.find_first_of( ">", pos);
}

void GameState::addPlayer( const std::string &description, size_t &pos){
	GameState::Player newPlayer;
	pos = description.find_first_of( "0123456789", pos);
	newPlayer.pos.x = std::stoi( std::string( description.begin() + pos, description.end()));
	pos = description.find_first_of( " \t", pos);	// pos += string.size()
	pos = description.find_first_of( "0123456789", pos);
	newPlayer.pos.y = std::stoi( std::string( description.begin() + pos, description.end()));
	pos = description.find_first_of( " \t", pos);
	pos = description.find_first_of( "0123456789", pos);
	newPlayer.color = std::stoi( std::string( description.begin() + pos, description.end()));
	pos = description.find_first_of( " \t", pos);
	pos = description.find_first_of( "0123456789", pos);
	newPlayer.headColor = std::stoi( std::string( description.begin() + pos, description.end()));
	pos = description.find_first_of( ">", pos);
	newPlayer.direction = "<up>";
	newPlayer.growth = 10;
	newPlayer.viewer = false;
	this->players.push_back( newPlayer);
}

bool GameState::collide( const GameState::Pos &pos){
	bool collision = false;
	std::vector< GameState::Player>::iterator player;
	for( player = this->players.begin(); player != this->players.end(); player++)
		collision = collision || player->collide( pos);
	return collision;
}

