#ifndef NetSnakeTron_UserInterface_H
#define NetSnakeTron_UserInterface_H

#include <string>
#include <SDL.h>
#include "Configuration.h"
#include "EventSender.h"
#include "StateHandler.h"

class UserInterface{
public:
	UserInterface( EventSender &sender, StateHandler &state, Configuration &conf);
	~UserInterface();
	UserInterface &waitThread();

	UserInterface &loop();
	static int display( void *data);
private:
	inline void log( const std::string &str, unsigned int type){ this->configuration.get_log().puts( str, type);}

	bool exit_signal;
	Configuration &configuration;
	EventSender &eventSender;
	StateHandler &stateHandler;
	SDL_Thread *sdlThread;
};

#endif//NetSnakeTron_UserInterface_H
