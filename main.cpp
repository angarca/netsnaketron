#include "NetSnakeTron.h"

int main( int argc, char *argv[]){
	std::string settings = "SETTINGS.conf";
	if( argc > 1) settings = argv[1];
	Configuration configuration( settings);
	NetSnakeTron game( configuration);

	game.loop();

	return 0;
}
