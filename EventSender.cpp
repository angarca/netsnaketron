#include "EventSender.h"

EventSender::EventSender( Configuration &conf): configuration(conf), ready(false){
	this->log( "EventSender::EventSender().\n", Log::TRACE);
	this->mutex = SDL_CreateMutex();
	this->message = "<>";
}

EventSender::~EventSender(){
	this->log( "EventSender::~EventSender().\n", Log::TRACE);
	if( this->mutex != nullptr) SDL_DestroyMutex( this->mutex);
}

bool EventSender::isReady(){
	this->log( "EventSender::isReady().\n", Log::TRACE);
	return this->ready;
}

EventSender &EventSender::setReady( bool b){
	this->ready = b;
}

std::string EventSender::getMessage(){
	this->log( "EventSender::getMessage().\n", Log::TRACE);
	std::string str;
	if( SDL_LockMutex( this->mutex) == 0){
		str = this->message;
		SDL_UnlockMutex( this->mutex);
		this->message = "<>";
		this->ready = false;
	}else this->log( "EventSender::getMessage(): Warning, the mutex could not be locked.\n", Log::WARNING);
	return str;
}

EventSender &EventSender::setMessage( const std::string &message){
	this->log( "EventSender::setMessage().\n", Log::TRACE);
	if( SDL_LockMutex( this->mutex) == 0){
		GameState::overrideInput( this->message, message, this->configuration.getAllowPause());
		SDL_UnlockMutex( this->mutex);
		this->log( std::string( "EventSender::setMessage(): message: ") + this->message + ".\n", Log::INFO);
	}else this->log( "EventSender::setMessage(): Warning, the mutex could not be locked.\n", Log::WARNING);	
	return *this;
}


