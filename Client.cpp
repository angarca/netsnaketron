#include "Client.h"

Client::Client( Configuration &conf): negotiation_done( false), state(Client::NONE), configuration(conf), serverTCPSocket(nullptr),
			serverSocketSet(nullptr), serverHandlerThread(nullptr),eventSender( conf), stateHandler( conf), inputs_V(nullptr){
	this->log( "Client::Client().\n", Log::TRACE);
}

Client::~Client(){
	this->log( "Client::~Client().\n", Log::TRACE);
	if( this->state & Client::IS_SERVER_SOCKET_CONECTED)
		this->stopConection();
	if( this->inputs_V != nullptr) delete[] inputs_V;
}

Client &Client::resolve( const std::string &ip, unsigned int port){
	this->log( "Client::resolve(): Trying to set up the client configuration.\n", Log::TRACE);
	if( SDLNet_ResolveHost( &(this->serverIP), ip.c_str(), port) == -1){
		this->log( std::string( "Client::resolve(): Error, ") + SDLNet_GetError(), Log::ERROR);
		exit(-1);
	}
	this->state |= Client::IS_SERVER_IP_INITIALIZED;
}

Client &Client::startConection(){
	this->log( "Client::startConection(): Trying to start the conection to the server.\n", Log::TRACE);
	if( this->state & Client::IS_SERVER_IP_INITIALIZED){
		this->serverTCPSocket = SDLNet_TCP_Open( &(this->serverIP));
		if( this->serverTCPSocket != nullptr){
			this->serverSocketSet = SDLNet_AllocSocketSet(1);
			SDLNet_TCP_AddSocket( this->serverSocketSet, this->serverTCPSocket);
			this->serverHandlerThread = SDL_CreateThread( Client::serverHandler, "Server handler thread", (void*)this);
			this->state |= Client::IS_SERVER_SOCKET_CONECTED;
		}else{
			this->log( std::string("Client::startConection(): Error, ") + SDLNet_GetError(), Log::ERROR);
			exit(-1);
		}
	}else this->log( "Client::startConection(): Warning, the client configuration is not set up.\n", Log::WARNING);
}

Client &Client::stopConection(){
	this->log( "Client::stopConection(): Trying to stop the conection to the server.\n", Log::TRACE);
	if( this->state & Client::IS_SERVER_SOCKET_CONECTED){
		this->state |= Client::EXIT_SIGNAL;
		SDL_WaitThread( this->serverHandlerThread, nullptr);
	}else this->log( "Client::stopConection(): Warning, the client wasn't listening.\n", Log::WARNING);
}

EventSender &Client::getEventSender(){
	return this->eventSender;
}

StateHandler &Client::getStateHandler(){
	while( !this->negotiation_done) SDL_Delay(1);	// the data inside the GameState is not valid until the end of the negotiation
	return this->stateHandler;
}

bool Client::is_exiting(){
	return this->state & Client::EXIT_SIGNAL;
}

int Client::serverHandler( void *data){		Client *virtual_this = (Client*)data;
	virtual_this->log( "Client::serverHandler(): Starting the server handler.\n", Log::INFO);
	const unsigned int LengthBufferInput = 1024;
	char input[LengthBufferInput];
	int readBytes, i, j;

	std::string tmp = virtual_this->stateHandler.configurationSummary();
	SDLNet_TCP_Send( virtual_this->serverTCPSocket, tmp.c_str(), tmp.size()+1);
	readBytes = SDLNet_TCP_Recv( virtual_this->serverTCPSocket, input, LengthBufferInput);
	if( readBytes > 0){
		virtual_this->stateHandler.initializePlayer( input);
		virtual_this->negotiation_done = true;
	}
	for( i=j=0; i<readBytes; i++) if( input[i] == '\0'){
		if( j != 0) virtual_this->processInput( input + j);
		j = i+1;
	}
	if( j != i)
		virtual_this->processInput( input + j);

	while( readBytes > 0 && !(virtual_this->state & Client::EXIT_SIGNAL)){
		if( readBytes > 0 && SDLNet_CheckSockets( virtual_this->serverSocketSet, 0) > 0){
			while( SDLNet_SocketReady( virtual_this->serverTCPSocket) != 0){
				readBytes = SDLNet_TCP_Recv( virtual_this->serverTCPSocket, input, LengthBufferInput);
				for( i=j=0; i<readBytes; i++) if( input[i] == '\0'){
					virtual_this->processInput( input + j);
					j = i+1;
				}
				if( j != i)
					virtual_this->processInput( input + j);
			}
		}
		if( readBytes > 0){
			if( virtual_this->eventSender.isReady()){
				const std::string &str = virtual_this->eventSender.getMessage();
				SDLNet_TCP_Send( virtual_this->serverTCPSocket, str.c_str(), str.size()+1);
			}
		}
		SDL_Delay(0);
	}
	virtual_this->state |= Client::EXIT_SIGNAL;	//
	SDLNet_TCP_DelSocket( virtual_this->serverSocketSet, virtual_this->serverTCPSocket);
	SDLNet_FreeSocketSet( virtual_this->serverSocketSet);
	SDLNet_TCP_Close( virtual_this->serverTCPSocket);
//	virtual_this->state &= ~(Client::IS_SERVER_SOCKET_CONECTED | Client::EXIT_SIGNAL);
	virtual_this->state &= ~(Client::IS_SERVER_SOCKET_CONECTED);
	virtual_this->log( "Client::serverHandler(): Stopped the server handler.\n", Log::INFO);
	return 0;
}

void Client::processInput( const std::string &input){
	size_t pos, end;
	unsigned int i;
	std::string str;
	if( (pos = input.find( "<update")) != std::string::npos){
		pos = input.find_first_of( "0123456789", pos);
		this->inputs_N = std::stoi( std::string( input.begin() + pos, input.end()));
		if( this->inputs_V != nullptr) delete[] this->inputs_V;
		this->inputs_V = new std::string[ this->inputs_N];
		this->eventSender.setReady();
		this->time = SDL_GetTicks();
	}else if( (pos = input.find( "<swap")) != std::string::npos){
		std::vector< std::string> inputs;
		for( i = 0; i < this->inputs_N; i++) inputs.push_back( this->inputs_V[i]);
		str = GameState::update( inputs);
		if( (pos = input.find( "<apple", pos)) != std::string::npos){
			end = input.find( ">", pos) + 1;
			str += std::string( input.begin() + pos, input.begin() + end);
		}
		this->stateHandler.setState( str);
	}else if( (pos = input.find( "<move")) != std::string::npos){
		pos = input.find_first_of( "0123456789", pos);
		i = std::stoi( std::string( input.begin() + pos, input.end()));
		str = "<ack " + std::to_string( i) + ">";
		SDLNet_TCP_Send( this->serverTCPSocket, str.c_str(), str.size()+1);
		pos = input.find( "<", pos);
		end = input.find( ">", pos) + 1;
		this->inputs_V[i] = std::string( input.begin() + pos, input.begin() + end);
	}else if( input == "<ack>"){
		this->time = SDL_GetTicks() - this->time;
		this->configuration.get_test().puts( std::to_string( this->time) + "\n", Log::INFO);
	}else if( input.find( "<player") != std::string::npos) this->stateHandler.setState( input);
	else if( input == "<pause>") this->stateHandler.setState( input);
	else;	// package not recognized.
}


