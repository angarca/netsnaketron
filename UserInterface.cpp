#include "UserInterface.h"

UserInterface::UserInterface( EventSender &sender, StateHandler &state, Configuration &conf): exit_signal(false), configuration(conf),
					eventSender( sender), stateHandler( state){
	this->log( "UserInterface::UserInterface().\n", Log::TRACE);
}

UserInterface::~UserInterface(){
	this->log( "UserInterface::~UserInterface().\n", Log::TRACE);
	if( this->sdlThread != nullptr) this->waitThread();
}

UserInterface &UserInterface::waitThread(){
	this->log( "UserInterface::waitThread().\n", Log::TRACE);
	if( this->sdlThread != nullptr){
		this->exit_signal = true;
		SDL_WaitThread( this->sdlThread, nullptr);
		this->sdlThread = nullptr;
	}
	return *this;
}

UserInterface &UserInterface::loop(){
	this->log( "UserInterface::loop().\n", Log::TRACE);
	SDL_InitSubSystem( SDL_INIT_VIDEO);
	this->sdlThread = SDL_CreateThread( UserInterface::display, "Display thread", (void*)this);
	SDL_Event event;

	bool exit_signal = false;
	while( !exit_signal){
		SDL_WaitEvent( &event);
		switch( event.type){
		case SDL_KEYDOWN:
			switch( event.key.keysym.sym){
			case SDLK_UP:
				this->eventSender.setMessage( "<up>");
				break;
			case SDLK_DOWN:
				this->eventSender.setMessage( "<down>");
				break;
			case SDLK_LEFT:
				this->eventSender.setMessage( "<left>");
				break;
			case SDLK_RIGHT:
				this->eventSender.setMessage( "<right>");
				break;
			case SDLK_RETURN:
				this->eventSender.setMessage( "<pause>");
				break;
			case SDLK_AC_BACK:	// android back button
			case SDLK_ESCAPE:
				exit_signal = true;
				break;
			default:;
			}
			break;
		case SDL_FINGERDOWN:
			if( event.tfinger.x > event.tfinger.y)
				if( event.tfinger.x > 1 - event.tfinger.y) this->eventSender.setMessage( "<right>");
				else this->eventSender.setMessage( "<up>");
			else
				if( event.tfinger.x > 1 - event.tfinger.y) this->eventSender.setMessage( "<down>");
				else this->eventSender.setMessage( "<left>");
			break;
		case SDL_QUIT:
			exit_signal = true;
			break;
		default:;
		}
	}

	if( this->sdlThread != nullptr) this->waitThread();
	SDL_QuitSubSystem( SDL_INIT_VIDEO);
	return *this;
}

int UserInterface::display( void *data){		UserInterface *virtual_this = (UserInterface*)data;
	virtual_this->log( "UserInterface::display().\n", Log::TRACE);
	Uint32 color;
	unsigned int w, h, **display, i, j;
	virtual_this->stateHandler.getSize( w, h);
	display = new unsigned int *[w];
	for( i=0; i<w; i++){
		display[i] = new unsigned int[h];
		for( j=0; j<h; j++) display[i][j] = GameState::BLACK;
	}

	SDL_Window *window = SDL_CreateWindow( "SnakeTron", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, w*10, h*10, 0);
	SDL_Renderer *renderer = SDL_CreateRenderer( window, -1, 0);
	SDL_Texture *texture = SDL_CreateTexture( renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, w*10, h*10);
	SDL_Surface *surface = SDL_CreateRGBSurface( 0, w*10, h*10, 32, 0, 0, 0, 0);
	SDL_SetRenderDrawColor( renderer, 0, 0, 0, 255);
	SDL_RenderClear( renderer);
	SDL_RenderPresent( renderer);

	while( !virtual_this->exit_signal){
		virtual_this->stateHandler.getDisplay( display, w, h);
		for( i=0; i<w*10; i++) for( j=0; j<h*10; j++){
			switch( display[ i/10][ j/10]){
			case GameState::RED:
				color = SDL_MapRGB( surface->format, 0xff, 0x0, 0x0);
				break;
			case GameState::GREEN:
				color = SDL_MapRGB( surface->format, 0x0, 0xff, 0x0);
				break;
			case GameState::BLUE:
				color = SDL_MapRGB( surface->format, 0x0, 0x0, 0xff);
				break;
			case GameState::SKY:
				color = SDL_MapRGB( surface->format, 0x0, 0xff, 0xff);
				break;
			case GameState::BROWN:
				color = SDL_MapRGB( surface->format, 0xff, 0x0, 0xff);
				break;
			case GameState::YELLOW:
				color = SDL_MapRGB( surface->format, 0xff, 0xff, 0x0);
				break;
			case GameState::LIGHT_RED:
				color = SDL_MapRGB( surface->format, 0xff, 0x7f, 0x7f);
				break;
			case GameState::LIGHT_GREEN:
				color = SDL_MapRGB( surface->format, 0x7f, 0xff, 0x7f);
				break;
			case GameState::LIGHT_BLUE:
				color = SDL_MapRGB( surface->format, 0x7f, 0x7f, 0xff);
				break;
			case GameState::LIGHT_SKY:
				color = SDL_MapRGB( surface->format, 0x7f, 0xff, 0xff);
				break;
			case GameState::LIGHT_BROWN:
				color = SDL_MapRGB( surface->format, 0xff, 0x7f, 0xff);
				break;
			case GameState::LIGHT_YELLOW:
				color = SDL_MapRGB( surface->format, 0xff, 0xff, 0x7f);
				break;
			case GameState::DARK_RED:
				color = SDL_MapRGB( surface->format, 0x7f, 0x0, 0x0);
				break;
			case GameState::DARK_GREEN:
				color = SDL_MapRGB( surface->format, 0x0, 0x7f, 0x0);
				break;
			case GameState::DARK_BLUE:
				color = SDL_MapRGB( surface->format, 0x0, 0x0, 0x7f);
				break;
			case GameState::DARK_SKY:
				color = SDL_MapRGB( surface->format, 0x0, 0x7f, 0x7f);
				break;
			case GameState::DARK_BROWN:
				color = SDL_MapRGB( surface->format, 0x7f, 0x0, 0x7f);
				break;
			case GameState::DARK_YELLOW:
				color = SDL_MapRGB( surface->format, 0x7f, 0x7f, 0x0);
				break;
			case GameState::WHITE:
				color = SDL_MapRGB( surface->format, 0xff, 0xff, 0xff);
				break;
			case GameState::GRAY:
				color = SDL_MapRGB( surface->format, 0x7f, 0x7f, 0x7f);
				break;
			case GameState::BLACK:
			default:
				color = SDL_MapRGB( surface->format, 0x0, 0x0, 0x0);
			}
			if( i%10 == 0 || i%10 == 9 || j%10 == 0 || j%10 == 9) color = SDL_MapRGB( surface->format, 0x0, 0x0, 0x0);
			*(Uint32*)(((Uint8*) surface->pixels) + i*4 + j*surface->pitch) = color;
		}

		SDL_UpdateTexture( texture, nullptr, surface->pixels, surface->pitch);
		SDL_RenderClear( renderer);
		SDL_RenderCopy( renderer, texture, nullptr, nullptr);
		SDL_RenderPresent( renderer);
		SDL_Delay( 30);
	}

	SDL_FreeSurface( surface);
	SDL_DestroyTexture( texture);
	SDL_DestroyRenderer( renderer);
	SDL_DestroyWindow( window);

	if( display != nullptr){
		for( i=0; i<w; i++) if( display[i] != nullptr) delete display[i];
		delete display;
	}

	return 0;
}


