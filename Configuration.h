#ifndef NetSnakeTron_Configuration_H
#define NetSnakeTron_Configuration_H

#include <iostream>

#include <string>
#include <fstream>
#include "Log.h"

class Configuration{
public:
	Configuration( const std::string &path);
	Configuration &save( const std::string &path);
	bool is_server(){ return this->server;}
	bool is_client(){ return this->client;}
	unsigned int get_port(){ return this->port;}
	const std::string &get_url(){ return this->url;}
	Log &get_log(){ return this->log;}
	Log &get_test(){ return this->test;}
	unsigned int get_timePerUpdate(){ return this->timePerUpdate;}
	unsigned int get_players(){ return this->players;}
	Configuration &set_players( unsigned int players){ this->players = players; return *this;}
	unsigned int get_games(){ return this->games;}
	Configuration &set_games( unsigned int games){ this->games = games; return *this;}
	unsigned int get_iterations(){ return this->iterations;}
	Configuration &get_dimensions( unsigned int &w, unsigned int &h){ w = this->W; h = this->H; return *this;}
	bool getAllowPause(){ return this->allowPause;}
	bool getBot(){ return this->bot;}
	bool getBlind(){ return this->blind;}

private:
	bool server, client;	// Is a server and/or a client
	unsigned int port;	// In server port to listen, in client port to connect
	std::string url;	// Should be use just to server

	Log log, test;
	unsigned int timePerUpdate;	// Only used on server, in mili-seconds
	unsigned int players, games;	// Only used on server: players per game, games before close (double->1, 0 means no limit).
	unsigned int iterations;	// Only used on server: iterations before closing a game (0 means no limit).
	unsigned int W, H;		// Only used on server
	bool allowPause;
	bool bot, blind;		// Only used on client
};

#endif//NetSnakeTron_Configuration_H


