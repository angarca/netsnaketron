#ifndef NetSnakeTron_GameHandler_H
#define NetSnakeTron_GameHandler_H

#include <vector>
#include <string>
#include <SDL.h>
#include <SDL_net.h>
#include "Configuration.h"
#include "GameState.h"
#include "ClientHandler.h"

class GameHandler{
public:
	GameHandler( Configuration &conf);
	~GameHandler();
	GameHandler &addClient( ClientHandler *client);
	GameHandler &start();
	GameHandler &pause();
	GameHandler &waitThread();
	bool is_on();

	static int thread( void *data);
private:
	void update();
	inline void log( const std::string &str, unsigned int type){ this->configuration.get_log().puts( str, type);}

	bool start_signal, exit_signal;
	Configuration &configuration;
	GameState gameState;
	std::vector< ClientHandler*> clients;
	SDL_Thread *sdlThread;
	SDL_mutex *mutex;
	unsigned int iterations;
};

#endif//NetSnakeTron_GameHandler_H
