#ifndef NetSnakeTron_Server_H
#define NetSnakeTron_Server_H

#include <vector>
#include <string>
#include <SDL.h>
#include <SDL_net.h>
#include "Configuration.h"
#include "ClientHandler.h"
#include "GameHandler.h"

class Server{
public:
	static const unsigned int NONE = 0x0, IS_INPUT_IP_INITIALIZED = 0x1, IS_INPUT_SOCKET_LISTENING = 0x2, EXIT_SIGNAL = 0x4;

	Server( Configuration &conf);
	~Server();
	Server &resolve( unsigned int port);
	Server &startListen();
	Server &stopListen();
	bool is_exiting();

	static int clientListener( void *data);
private:
	inline void log( const std::string &str, unsigned int type){ this->configuration.get_log().puts( str, type);}

	unsigned int state;
	Configuration &configuration;
	IPaddress inputIP;
	TCPsocket inputTCPSocket;
	SDL_Thread *clientListenerThread;
};

#endif//NetSnakeTron_Server_H

