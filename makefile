.PHONY: todo server clients exe

CPP=g++
SDL_LIBS=`sdl2-config --libs` -lSDL2_net
SDL_CFLAGS=`sdl2-config --cflags`

todo: main

exe: main
	-./main double.conf &

server: todo
	-./main server.conf

clients: todo
	-./main client1.conf &
	sleep 2
	-./main client2.conf &
	sleep 2
	-./main client3.conf &

objects :=

objects := $(objects) Log.o
Log.o: Log.cpp Log.h
	$(CPP) -c -std=c++11 Log.cpp -o Log.o

Configuration.h: Log.h
	touch Configuration.h

objects := $(objects) Configuration.o
Configuration.o: Configuration.cpp Configuration.h
	$(CPP) -c -std=c++11 Configuration.cpp -o Configuration.o

GameState.h: Configuration.h
	touch GameState.h

objects := $(objects) GameState.o
GameState.o: GameState.cpp GameState.h
	$(CPP) -c -std=c++11 GameState.cpp -o GameState.o $(SDL_CFLAGS)

ClientHandler.h: Configuration.h GameState.h
	touch ClientHandler.h

objects := $(objects) ClientHandler.o
ClientHandler.o: ClientHandler.cpp ClientHandler.h
	$(CPP) -c -std=c++11 ClientHandler.cpp -o ClientHandler.o $(SDL_CFLAGS)

GameHandler.h: Configuration.h GameState.h ClientHandler.h
	touch GameHandler.h

objects := $(objects) GameHandler.o
GameHandler.o: GameHandler.cpp GameHandler.h
	$(CPP) -c -std=c++11 GameHandler.cpp -o GameHandler.o $(SDL_CFLAGS)

Server.h: Configuration.h ClientHandler.h GameHandler.h
	touch Server.h

objects := $(objects) Server.o
Server.o: Server.cpp Server.h
	$(CPP) -c -std=c++11 Server.cpp -o Server.o $(SDL_CFLAGS)

EventSender.h: Configuration.h
	touch EventSender.h

objects := $(objects) EventSender.o
EventSender.o: EventSender.cpp EventSender.h
	$(CPP) -c -std=c++11 EventSender.cpp -o EventSender.o $(SDL_CFLAGS)

StateHandler.h: Configuration.h GameState.h
	touch StateHandler.h

objects := $(objects) StateHandler.o
StateHandler.o: StateHandler.cpp StateHandler.h
	$(CPP) -c -std=c++11 StateHandler.cpp -o StateHandler.o $(SDL_CFLAGS)

Client.h: Configuration.h EventSender.h StateHandler.h
	touch Client.h

objects := $(objects) Client.o
Client.o: Client.cpp Client.h
	$(CPP) -c -std=c++11 Client.cpp -o Client.o $(SDL_CFLAGS)

UserInterface.h: Configuration.h EventSender.h StateHandler.h
	touch UserInterface.h

objects := $(objects) UserInterface.o
UserInterface.o: UserInterface.cpp UserInterface.h
	$(CPP) -c -std=c++11 UserInterface.cpp -o UserInterface.o $(SDL_CFLAGS)

NetSnakeTron.h: Configuration.h Server.h Client.h UserInterface.h
	touch NetSnakeTron.h

objects := $(objects) NetSnakeTron.o
NetSnakeTron.o: NetSnakeTron.cpp NetSnakeTron.h
	$(CPP) -c -std=c++11 NetSnakeTron.cpp -o NetSnakeTron.o $(SDL_CFLAGS)

objects := $(objects) main.o
main.o: main.cpp NetSnakeTron.h
	$(CPP) -c -std=c++11 main.cpp -o main.o $(SDL_CFLAGS)

main: $(objects)
	$(CPP) $(objects) -o main $(SDL_LIBS)


