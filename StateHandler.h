#ifndef NetSnakeTron_StateHandler_H
#define NetSnakeTron_StateHandler_H

#include <string>
#include <SDL.h>
#include "Configuration.h"
#include "GameState.h"

class StateHandler{
public:
	StateHandler( Configuration &conf);
	~StateHandler();
	std::string configurationSummary();
	StateHandler &initializePlayer( const std::string &grantedConfiguration);
	StateHandler &getSize( unsigned int &w, unsigned int &h);
	StateHandler &getDisplay( unsigned int **, unsigned int w, unsigned int h);
	StateHandler &setState( const std::string &update);
private:
	inline void log( const std::string &str, unsigned int type){ this->configuration.get_log().puts( str, type);}

	Configuration &configuration;
	GameState *gameState[2];
	unsigned int stateDisplayable, stateUpdateable;
	SDL_mutex *mutex;
};

#endif//NetSnakeTron_StateHandler_H

