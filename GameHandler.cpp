#include "GameHandler.h"

GameHandler::GameHandler( Configuration &conf): start_signal(false), exit_signal(false), configuration(conf), gameState(conf){
	this->log( "GameHandler::GameHandler().\n", Log::TRACE);
	this->mutex = SDL_CreateMutex();
	this->sdlThread = SDL_CreateThread( GameHandler::thread, "Game handler", (void*)this);
}

GameHandler::~GameHandler(){
	this->log( "GameHandler::~GameHandler().\n", Log::TRACE);	
	if( this->sdlThread != nullptr) this->waitThread();
	if( this->mutex != nullptr) SDL_DestroyMutex( this->mutex);
}

GameHandler &GameHandler::addClient( ClientHandler *client){
	this->log( "GameHandler::addClient().\n", Log::TRACE);
	if( SDL_LockMutex( this->mutex) == 0){
		std::string newPlayerMessage;
		client->addOutput( this->gameState.addPlayer( client->getInput(), newPlayerMessage));
		for( std::vector< ClientHandler*>::iterator it = this->clients.begin(); it != this->clients.end(); it++)
			(*it)->addOutput( newPlayerMessage);
		this->clients.push_back( client);
		SDL_UnlockMutex( this->mutex);
	}else this->log( "GameHandler::addClient(): Warning, could not lock the mutex.\n", Log::WARNING);
	return *this;
}

GameHandler &GameHandler::start(){
	this->start_signal = true;
	this->iterations = 0;
	this->pause();
}

GameHandler &GameHandler::pause(){
	this->log( "GameHandler::pause().\n", Log::TRACE);
	if( SDL_LockMutex( this->mutex) == 0){
		for( std::vector< ClientHandler*>::iterator it = this->clients.begin(); it != this->clients.end(); it++)
			(*it)->addOutput( "<pause>");
		this->gameState.update( "<pause>");
		SDL_UnlockMutex( this->mutex);
	}else this->log( "GameHandler::pause(): Warning, could not lock the mutex.\n", Log::WARNING);
	return *this;
}

GameHandler &GameHandler::waitThread(){
	this->log( "GameHandler::waitThread().\n", Log::TRACE);
	if( !(this->exit_signal)){
		this->exit_signal = true;
		SDL_WaitThread( this->sdlThread, nullptr);
		this->sdlThread = nullptr;
	}else this->log( "GameHandler::waitThread(): Warning, waitThread has already been called.\n", Log::WARNING);
	return *this;
}

bool GameHandler::is_on(){
	return !this->exit_signal;
}

int GameHandler::thread( void *data){		GameHandler *virtual_this = (GameHandler*)data;
	virtual_this->log( "GameHandler::thread().\n", Log::TRACE);
	std::vector< ClientHandler*>::iterator it;
	while( !(virtual_this->start_signal || virtual_this->exit_signal)) SDL_Delay(1);
	while( !(virtual_this->exit_signal)){
		if( SDL_LockMutex( virtual_this->mutex) == 0){
			if( !virtual_this->clients.empty())
				virtual_this->update();
			SDL_UnlockMutex( virtual_this->mutex);
		}else virtual_this->log( "GameHandler::thread(): Warning, could not lock the mutex.\n", Log::WARNING);
		SDL_Delay( virtual_this->configuration.get_timePerUpdate());
		if( virtual_this->configuration.get_iterations() != 0)
			if( ++(virtual_this->iterations) >= virtual_this->configuration.get_iterations())
				virtual_this->exit_signal = true;
	}
	if( SDL_LockMutex( virtual_this->mutex) == 0){
		for( it = virtual_this->clients.begin(); it != virtual_this->clients.end(); it++){
			(*it)->waitThread();
			delete *it;
		}
		SDL_UnlockMutex( virtual_this->mutex);
	}else virtual_this->log( "GameHandler::thread(): Warning, could not lock the mutex.\n", Log::WARNING);
	return 0;
}

void GameHandler::update(){
	size_t pos;
	std::string str, apple;
	std::vector< std::string> inputs;
	std::vector< ClientHandler*>::iterator it, it2;
	unsigned int size = this->clients.size(), i, j, k;
	bool answer_all, *answers = new bool[ size], **acks = new bool*[ size];
	std::string *inputs_V = new std::string[ size];
	for( it = this->clients.begin(), i = 0; it != this->clients.end(); it++, i++){
		(*it)->addOutput( "<update " + std::to_string(size) + ">");
		answers[i] = false;
		acks[i] = new bool[ size];
	}
	do{
		for( it = this->clients.begin(), i = 0; it != this->clients.end(); it++, i++){
			//if( (*it)->is_socket_connected()){	// detectar, y manejar desconexión.
			if( (*it)->isInputReady()){
				str = (*it)->getInput();
				if( (pos = str.find( "<ack")) != std::string::npos){
					pos = str.find_first_of( "0123456789", pos);
					j = std::stoi( std::string( str.begin() + pos, str.end()));
					acks[j][i] = true;
					for( k = 0, answer_all = true; k < size; k++) answer_all = answer_all && acks[j][k];
					if( answer_all) for( it2=this->clients.begin(), k=0; it2 != this->clients.end(); it2++, k++)
						if( j == k) (*it2)->addOutput( "<ack>");
				}else{
					//if( answers[i]) // se ha recivido más de un paquete de este cliente en el mismo ciclo //malo
					inputs_V[i] = str;
					answers[i] = true;
					for( j = 0; j < size; j++) acks[i][j] = false;
					for( it2 = this->clients.begin(), j=0; it2 != this->clients.end(); it2++, j++){
						(*it2)->addOutput( "<move " + std::to_string(i) + inputs_V[i] + ">");
}
				}
			}
		}
		for( i = 0, answer_all = true; i < size; i++){
			answer_all = answer_all && answers[i];
			for( j = 0; j < size; j++)
				answer_all = answer_all && acks[i][j];
		}
	}while( !answer_all);
	for( i = 0; i < size; i++){
		inputs.push_back( inputs_V[i]);
		delete[] acks[i];
	}
	if( size > 0){
		str = GameState::update( inputs);
		this->gameState.update( str);
		apple = this->gameState.generateApple();
	}else this->exit_signal = true;
	delete[] inputs_V;
	delete[] answers;
	delete[] acks;
	for( it = this->clients.begin(); it != this->clients.end(); it++){
		(*it)->addOutput( "<swap " + apple + ">");
	}
}
