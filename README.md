# README #

This is a simple snake-like game written in SDL, is a networking version of https://bitbucket.org/angarca/snaketron

To use it, first you need to have installed http://libsdl.org/download-2.0.php and https://www.libsdl.org/projects/SDL_net/ under:
    /usr/local/include/SDL2
    /usr/local/lib

Then just use make to compile it, ./main to start a server and ./main <server_IP> to start a client.
To see a example running: make server, launch the server, that locks the terminal until you type 'exit' which closes the server, and: make exe, launch four clients.

For the moment the number of players at every game is hard-coded to 2, the port of the server is hard-coded to 9999, the control keys are hard-coded to the arrow keys, the snakes color is hard-coded to red with the head white, and the apple to green, the initial length is hard-coded to 10 tiles, the growth per apple is hard-coded to 5, the position of the first apple is hard-coded at the center of the screen, the position of every new apple is hard-coded to 15 tiles right to the last position, the update speed is hard-coded to 1 fps, the game size is hard-coded to 64 X 48 tiles, the window resolution is hard-coded to 640X480, the escape key close the client, and there are still plenty of bugs since is just a prototype under construction.