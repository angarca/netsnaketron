#include "Log.h"

Log::Log( unsigned int levels){
	this->levels = levels;
}

Log &Log::setFile( const std::string &file){
	this->fileName = file;
	return *this;
}

const std::string &Log::getFile(){
	return this->fileName;
}

Log &Log::setLevel( unsigned int levels){
	this->levels = levels;
	return *this;
}

Log &Log::puts( const std::string &str, unsigned int levels){
 	std::ofstream file(fileName.c_str(), std::ofstream::app);	// android compiler doesn't support fstream( string)
	if(file.is_open() && this->levels & levels){
		file << str;
		file.close();
	}
	return *this;
}


