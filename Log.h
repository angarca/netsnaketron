#ifndef NetSnakeTron_Log_H
#define NetSnakeTron_Log_H

#include <string>
#include <fstream>

class Log{
public:
	static const unsigned int NONE = 0x0, ERROR = 0x1, WARNING = 0x2, DEPURATION = 0x4, TRACE = 0x8, INFO = 0x16;

	Log( unsigned int levels = NONE);
	Log &setFile( const std::string &file);
	const std::string &getFile();
	Log &setLevel( unsigned int levels);
	Log &puts( const std::string &str, unsigned int levels);
private:
	std::string fileName;
	unsigned int levels;
};

#endif//NetSnakeTron_Log_H

